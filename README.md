# EDP Versioning Portal

Based on Gitea


## Background

Experimental task around git based dataset management


## Installation

Install Docker compose
pacman -S docker-compose

Start Docker service	
sudo systemctl start docker

Add user to docker group
gpasswd -a peter docker -->

cd gitea
docker-compose up -d

Should now be accessible at localhost:3000


## TODO:

* Saving DCAT creates a Pull Request
* CSV to RDF
* raise issue if commit did not conform to {DCAT/MQA/SHACL}

DONE:
X add "About Dataset" tab for repository which just shows README.md
X add "Metadata" tab
  X Tabulate DCAT 
X add "Download" tab
  * download CSV (RDF to CSV conversion)
  * download RDF (CSV to RDF conversion)



SHACL Validation
https://gitlab.com/european-data-portal/mqa/shacl-validation
Microservice for validating RDF graphs based on SHACL.


Glossary
* SHACL = Shapes Constraint Language
* DCAT = describe datasets and data services
* MQA = Metadata Quality Aassurances

Each repository has:
* Data: CSV format
* DCAT 1/2 description in N-Triples format (NT) file